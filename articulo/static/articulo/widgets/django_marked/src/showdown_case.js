import showdown from "showdown";
 
document.addEventListener(
  'DOMContentLoaded',
    () => {
        const editor_id = "editor";
        const result_id = "resultado";
        let editor =document.getElementById(editor_id);
        let result =document.getElementById(result_id);

        let options = {
            headerLevelStart:3,
            parseImgDimensions:true,
            simplifiedAutoLink:true,            
            tables:true,
            tablesHeaderId:true,
            ghCodeBlocks:true,
            tasklists:true,
            requireSpaceBeforeHeadingText: true,
            encodeEmails:true,
            openLinksInNewWindow:true,
            noHeaderId: true,
            customizedHeaderId: true,
            ghCompatibleHeaderId: true,
            omitExtraWLInCodeBlocks:false,
            extensions: [
                showdownKatex({
                    // maybe you want katex to throwOnError
                    throwOnError: true,
                    // disable displayMode
                    displayMode: false,
                    // change errorColor to blue
                    errorColor: '#1500ff',
                }),
                'icon'
            ],
        };
        let converter = new showdown.Converter(options);
  },
);

