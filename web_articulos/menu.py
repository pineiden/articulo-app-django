def base_link(data):
    items = ("title", "href", "text")
    return dict(
        zip(
            items,
            (data[0:len(items)])
        )
    )


def menu_nav(**kwargs):
    main_menu = [
        base_link(('Inicio', '/', "Inicio")),
        base_link(('Articulo',
                   'articulo/crear',
                   "Articulo")),
        base_link(('Nosotros',
                   '/pagina/nosotros',
                   "Nosotros")),
        base_link(('Proyectos',
                   '/proyecto/listar',
                   "Proyectos")),
        base_link(('Inscribirse',
                   '/inscripcion/activas',
                   "Inscribirse")),
        base_link(('Contacto',
                   '/contacto',
                   "Contacto")),
    ]

    return main_menu

MENU_NAV=menu_nav()
