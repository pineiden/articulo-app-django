"""
Date control
"""
USE_TZ=True

DATE_CONTROL = {
    'fecha_creacion':0,
    'fecha_fin_borrador':3,
    'fecha_aprobacion':7,
    'fecha_publicacion':8,
    'fecha_cancelacion':None
}

from datetime import datetime, timedelta

def date_control_params(dt, control=DATE_CONTROL):
    return {field:dt+timedelta(days=control.get(key))
            for field, value in DATE_CONTROL.items()
            if control.get(key)}
